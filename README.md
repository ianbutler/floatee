## Floatee

Floatee is a donation web-app that let's people "subscribe" to businesses or people who you would normally help or give patronage to but, in the wake of COVID19 are not able to. The goal is to help businesses stay a-float, hence Floatee. 


## Goals

- [ ] Localized search for those being donated to
- [ ] Re-ocurring donations to those being donated to 
- [ ] Message posting from those being donated to
- [ ] Stats dashboard for those being donated to 
- [ ] Individual pages with custom link for those being donated to
- [ ] Allow those being donated to offer virtual goods in return -- e.g. gift cards or vouchers
