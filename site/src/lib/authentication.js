import sarequest from "superagent";

export const authenticate = (creds, token) => {
    var route = ""
    var payload = null
    const {email, password} = creds

    if(!(email !== "" && password !== "") && !token) {
        signOut();
    } else if(creds) {
        route = "/auth/login"
        payload = creds
    } else if(token) {
        route = "/auth/login"
        payload = token
    }

    return sarequest.post(process.env.REACT_APP_API_SERVER + route + payload)
        .query({redirect_uri: window.location.origin})
        .on('error', (err) => {
            signOut();
        }).then((res) => {
            localStorage.setItem("accessToken", res.body.access_token);
            localStorage.setItem("userId", res.body.user_id);
            setTimeout(function() { authenticate(null, res.body.access_token) }, ((res.body.expires_in-400) * 1000));

            return true;
        }).catch((res) => {
            signOut();
        })
}

const clearAuthenticationCredentials = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("idToken");
    localStorage.removeItem("userId");
    localStorage.removeItem("acceptedTerms")
}

const directToLoginPage = () => {
    window.location.assign("/auth");
}

const directToLogoutPage = () => {
    window.location.assign("/auth");
}

export const signOut = () => {
    clearAuthenticationCredentials();
    directToLogoutPage();
}
