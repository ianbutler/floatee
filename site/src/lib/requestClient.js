import sarequest from "superagent";
import { authenticate } from "./authentication";

var unauthorizedRedirect = function (req) {
  req.on('response', function (res) {
    if (res.status === 401) {
      window.location.assign("/auth")
    }
  });
};

var alertsMiddleware = function (alertHandler, message) {
  return (req) => {
    req.on('response', function (res) {
      if (res.status < 400) {
        const alert = message || res.body || res.text;
        alertHandler.success(alert);
      } else if (res.status > 401 || res.status === 400) {
        let alert = "Server had a hiccup try again in a bit.";

        if (res.body || res.text) {
          alert = res.text || res.body.reason || alert;
        }

        alertHandler.error(alert);
      }
    });
  };
};

const request = {
  defaults: (request, alertHandler = null, message = null) => {

    const token = localStorage.getItem("idToken");

    if (token && request.method !== "OPTIONS") {
      request.set('Authorization', "Bearer " + token);
    }

    request.use(unauthorizedRedirect);

    if (alertHandler) {
      request.use(alertsMiddleware(alertHandler, message));
    }

    return request;
  },
  call: (method, url, alertHandler = null, message = null) => {
    return request.defaults(sarequest[method](url), alertHandler, message)
  }
}

export default request;
