import {
  drawerWidth,
  transition,
  container
} from "assets/jss/material-dashboard-react.js";

const appStyle = theme => ({
  wrapper: {
    overflowY: "hidden",
    [theme.breakpoints.down('sm')]: {
      maxHeight: "-webkit-fill-available",
    },
    [theme.breakpoints.up('md')]: {
      height: "100vh",
    },
    position: "relative",
    top: "0",
  },
  mainPanel: {
    overflow: "auto",
    position: "relative",
    ...transition,
    height: "100%",
    width: "100%",
    overflowScrolling: "touch",
    display: "flex",
    flexDirection: "column"
  },
  content: {
    flex: "1 0 75%",
    padding: "30px 15px",
    [theme.breakpoints.up('md')]: {
      marginTop: "75px"
    },
  },
  container,
  map: {
    marginTop: "70px"
  }
});

export default appStyle;
