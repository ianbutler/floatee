// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import Settings from "@material-ui/icons/Settings";
import LockOpen from "@material-ui/icons/LockOpen"
// core components/views for Recipient layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import SettingsView from "views/Settings/SettingsView.js";
import LoginSignupView from "views/LoginSignup/LoginSignupView.js";

export const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/recipient"
  },
  {
    path: "/profile",
    name: "Profile",
    icon: Person,
    component: UserProfile,
    layout: "/recipient"
  },
  {
    path: "/settings",
    name: "Settings",
    icon: Settings,
    component: SettingsView,
    layout: "/recipient"
  },
];

export const allRoutes = [
  {
    path: "/auth",
    name: "Auth",
    icon: LockOpen,
    component: LoginSignupView,
    layout: "/auth"
  }
].concat(dashboardRoutes)
