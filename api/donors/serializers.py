from rest_framework import serializers
from users.serializers import UserSerializer
from .models import Donation


class DonationSerializer(serializers.ModelSerializer):
    class Meta:
        model = "Donation"
        fields = ["amount", "donor", "recipient", "created_at", "updated_at"]

    def create(self, validated_data):
        donation = Donation(**validated_data)
        donation.save()
        return donation

    def validate(self, data):
        if data["donor"] == data["recipient"]:
            raise serializers.ValidationError("You cannot donate to yourself.")

        return data


class DonorSerializer(UserSerializer):
    pass
