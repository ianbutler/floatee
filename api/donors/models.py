import uuid

from django.db import models
from users.models import Account


class Donation(models.Model):
    external_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    amount = models.FloatField(null=False)
    donor = models.ForeignKey(Account, related_name="donor", on_delete=models.PROTECT)
    recipient = models.ForeignKey(
        Account, related_name="recipient", on_delete=models.PROTECT
    )
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(default=None, null=True)
    is_deleted = models.BooleanField(default=False, null=False)
