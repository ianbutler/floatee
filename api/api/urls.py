"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import include, path
from django.contrib import admin
from rest_framework import routers
from users.views import FacebookLogin, GoogleLogin, AccountViewSet
from recipients.views import OfferingViewSet, GiftViewSet
from goods.views import GoodsViewSet

router = routers.DefaultRouter()
router.register("account", AccountViewSet)
router.register("offering", OfferingViewSet)
router.register("gift", GiftViewSet)
router.register("good", GoodsViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    path('auth/registration/', include('rest_auth.registration.urls')),
    path('auth/facebook/', FacebookLogin.as_view(), name='fb_login'),
    path('auth/google/', GoogleLogin.as_view(), name='gg_login'),
    path("stripe/", include("djstripe.urls", namespace="djstripe")),
]
