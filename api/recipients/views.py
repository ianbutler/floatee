from django.shortcuts import render
from view_sets import SoftDeleteModelViewSet
from .models import Gift, Offering
from .serializers import GiftSerializer, OfferingSerializer


class GiftViewSet(SoftDeleteModelViewSet):
    lookup_field = "external_id"
    queryset = Gift.objects.all()
    serializer = GiftSerializer


class OfferingViewSet(SoftDeleteModelViewSet):
    lookup_field = "external_id"
    queryset = Offering.objects.all()
    serializer = OfferingSerializer
