from rest_framework import mixins, viewsets
from datetime import datetime


class UpdateRetrieveViewSet(
    mixins.UpdateModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    """
    A viewset that provides `retrieve`, and `update` actions.

    To use it, override the class and set the `.queryset` and
    `.serializer_class` attributes.
    """

    pass


class SoftDeleteModelViewSet(viewsets.ModelViewSet):
    def destroy(self, request, pk=None):
        record = self.get_object()
        record.deleted_at = datetime.now()
        record.is_deleted = True
        record.save()

